import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';

export const component = [
	LoginFormComponent,
	RegisterFormComponent,
]

@NgModule({
	declarations: component,
	imports: [
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		CommonModule
	],
	exports: component
})
export class ComponentsModule {}
