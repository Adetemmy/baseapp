import { Component, OnInit } from '@angular/core';
// import { FirebaseAuthProvider } from '../providers/firebase/firebaseauth';
// import { firestore } from 'firebase';
import { FirebaseDBProvider } from '../providers/firebase/firebasedb';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.page.html',
  styleUrls: ['./inventory.page.scss'],
})
export class InventoryPage implements OnInit {

  public data ={
    category: '',
    sub: '',
    price: '',
    discount: '',
    color: '',
    description: '',
  }
  constructor(public db: FirebaseDBProvider) { }

  ngOnInit() {
  }

  onSubmit(){
    this.db.addFireStoreData('user', this.data).then(data => console.log(data)).catch(err =>console.log(err))
  }
}
