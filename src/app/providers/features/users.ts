import { Injectable, OnInit } from '@angular/core';
import { FirebaseAuthProvider } from '../firebase/firebaseauth';
import { FirebaseDBProvider} from '../firebase/firebasedb';
import * as firebase from 'firebase/app';
import 'firebase/auth';

@Injectable()
export class UserProvider {
   constructor( private auth: FirebaseAuthProvider, private firebasedb: FirebaseDBProvider ){    
   }

   setDataForm(form, any){
     console.log(form);
   }

   async register(form: any){
    return await new Promise((resolve, reject) => {
      this.auth.signUp("email", form)
      .then((res: any) => {
          let details: any = {
            email: form.email,
            uid: this.auth.currentUser().currentUser.uid,
            firstname: 'form.firstname',
            lastname: '',
            sex: '',
            mobile: '',
            photoURL: '',
            address: '',
            isVendor: true,
            country: '',
            dateAdded: new Date().getUTCDate(),
            dateofbirth: '',
            state: '',
            othername: '',
            providerId: this.auth.currentUserProviderData()[0].providerId,
            firstTime: true
          }
          let formData = [];
          for(let i in details) {
            formData[i] = details[i]
          }
          this.firebasedb.saveFireData("users/" + this.auth.currentUser().currentUser.uid, formData)
          .then((res: any) => {
            resolve("user created");
          })
          .catch((err: any) => {
            reject(err.message || err.error || err);
          })  
      })
      .catch((err: any) => {
        reject(err.message || err.error || err);
      })  
    })
   }

}