import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/auth';

@Injectable()

export class FirebaseAuthProvider {
  loading: any;
  firebase: any = firebase;

  constructor(
  ) {
  }

  public checkAuthState = (callback: any) => {
    firebase.auth().onAuthStateChanged(
      (user) => {
        callback(user);
      },
      (error) => {
        callback(error);
      }
    );
  }

  currentUserProviderData(): any[] {
    return firebase.auth().currentUser.providerData;
  }
  currentUser(): any {
    return firebase.auth();
  }

  signOut = () => {
    return firebase.auth().signOut();
  }

  public async signIn(form: any, type: string) {
    switch (type) {
      case "email":
        return firebase.auth().signInWithEmailAndPassword(form.email, form.password);
      default:
    }
  }

  public async signUp(type: string, form: any) {
    switch (type) {
      case "email":
        return await firebase.auth().createUserWithEmailAndPassword(form.email, form.password);
      default:
    }
  }

  public updateProfile(form: any) {
    return firebase.auth().currentUser.updateProfile({
      displayName: form.displayName,
      photoURL: form.photoURL
    });
  }

  public async resetPassword(form: any) {
    return await firebase.auth().sendPasswordResetEmail(form.email);
  }

}
