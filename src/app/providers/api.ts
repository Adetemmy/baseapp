import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
declare var $: any;


@Injectable()

export class ApiProvider 
{
    url: string = "https://my.api.mockaroo.com/";
    constructor() {

    }

    async getData(path: string): Promise<any> {
        return await new Promise((resolve, reject) => {
            fetch(`${this.url}${path}.json?key=599c42f0`, {
                method: "GET"
            })
            .then( res => res.json())
            .then((data: any) => {
                resolve(data)
            })
            .catch((error: any) => {
                reject(error);
            });
        });
    }

}
  