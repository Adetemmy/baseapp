import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { firebaseConfig } from './config/configuration';

import * as firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database';
import 'firebase/storage';

// Providers import
import { FirebaseAuthProvider} from 'src/app/providers/firebase/firebaseauth';
import { UserProvider }  from 'src/app/providers/features/users'
import { FirebaseDBProvider } from 'src/app/providers/firebase/firebasedb';

firebase.initializeApp(firebaseConfig);

import { ApiProvider } from './providers/api';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiProvider,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, FirebaseAuthProvider, UserProvider, FirebaseDBProvider,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
