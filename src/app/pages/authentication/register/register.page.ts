import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserProvider } from 'src/app/providers/features/users'
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  loader: boolean = false;
  constructor(
    private UserProvider: UserProvider,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
  }

  registerForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(".+\@.+\...+")]),
    fullname: new FormControl('', Validators.required),
    password: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(30)
    ])),
    // phone: new FormControl('', [Validators.required,Validators.minLength(11)]),
    rpassword: new FormControl('',Validators.required),
    terms: new FormControl('', Validators.pattern('true'))
  }, { 
    validators: this.password.bind(this)
  });
  
  error_messages = {
    'firstname': [
      { type: 'required', message: 'First Name is required.' },
    ],

    'email': [
      { type: 'minlength', message: 'Email min length is 6.' },
      { type: 'maxlength', message: 'Email max length is 30.' },
      { type: 'required', message: 'please enter a valid email address.' }
    ],

    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' }
    ],
    'rpassword': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
    
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('rpassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  async register(form: any) {
    // This code brings in loader
    let loader = await this.loadingCtrl.create({
      message: "Creating your account .."
    });
    loader.present();
    // end of loader
    this.UserProvider.register(form)
    .then((results: any) => {
      loader.dismiss();
      console.info("Success ", results);
    })
    .catch((err: any) => {
      console.error("Register error ", err);
      loader.dismiss();
    });
  }

}
