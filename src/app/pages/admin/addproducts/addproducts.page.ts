import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-addproducts',
  templateUrl: './addproducts.page.html',
  styleUrls: ['./addproducts.page.scss'],
})
export class AddproductsPage implements OnInit {
  savedChanges=false;

  constructor() { }

  ngOnInit() {
  }

  addDataForm: FormGroup= new FormGroup({
    price: new FormControl('',[ Validators.required, Validators.pattern('[0-9]+')]),
    discount: new FormControl('',[ Validators.required, Validators.pattern('[0-9]+')]),
    category: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    tags: new FormControl('', Validators.required),
    colour: new FormControl('', Validators.required),
    scategory: new FormControl('', Validators.required),
    descr: new FormControl('', Validators.required),
    
  })
}
