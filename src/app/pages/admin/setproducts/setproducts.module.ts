import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetproductsPageRoutingModule } from './setproducts-routing.module';

import { SetproductsPage } from './setproducts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SetproductsPageRoutingModule
  ],
  declarations: [SetproductsPage]
})
export class SetproductsPageModule {}
