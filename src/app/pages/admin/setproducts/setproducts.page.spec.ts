import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetproductsPage } from './setproducts.page';

describe('SetproductsPage', () => {
  let component: SetproductsPage;
  let fixture: ComponentFixture<SetproductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetproductsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetproductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
