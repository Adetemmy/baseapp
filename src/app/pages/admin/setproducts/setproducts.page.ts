import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-setproducts',
  templateUrl: './setproducts.page.html',
  styleUrls: ['./setproducts.page.scss'],
})
export class SetproductsPage implements OnInit {
    toggleField: string;
    savedChanges=false;
    error: boolean=false;
    errorMessage: string="";
    dataloading: boolean=false;
    private querysubscription;
  constructor() { }

  ngOnInit() {
    this.toggleField = "searchMode";
  }

  toggle(filter?){
    if (!filter){filter="searchMode"}
    else{filter=filter;}
    this.toggleField = filter;
  }
  searchFormData:FormGroup = new FormGroup ({
    category: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
  })

  addDataForm: FormGroup= new FormGroup({
    price: new FormControl('',[ Validators.required, Validators.pattern('[0-9]+')]),
    discount: new FormControl('',[ Validators.required, Validators.pattern('[0-9]+')]),
    category: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    tags: new FormControl('', Validators.required),
    colour: new FormControl('', Validators.required),
    scategory: new FormControl('', Validators.required),
    
  })

}
